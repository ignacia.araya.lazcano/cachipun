import random
import sys

opciones = ["piedra", "papel", "tijera"]
computador = random.choice(opciones)

jugada = sys.argv[1]

if jugada in opciones: 
    if computador == jugada:
        print(f"Tú jugaste {jugada}\nComputador jugó {computador}\nEmpate!!")
    elif computador == "piedra":
        if jugada == "tijera" :
            print(f"Tú jugaste {jugada}\nComputador jugó {computador}\nPerdiste!!")
        else:
            print(f"Tú jugaste {jugada}\nComputador jugó {computador}\nGanaste!!")
    elif computador == "papel":
        if jugada == "tijera" :
            print(f"Tú jugaste {jugada}\nComputador jugó {computador}\nGanaste!!")
        else:
            print(f"Tú jugaste {jugada}\nComputador jugó {computador}\nPerdiste!!")
    elif computador == "tijera":
        if jugada == "papel" :
            print(f"Tú jugaste {jugada}\nComputador jugó {computador}\nPerdiste!!")
        else:
            print(f"Tú jugaste {jugada}\nComputador jugó {computador}\nGanaste!!")
else:
    print("Argumento inválido: Debe ser piedra, papel o tijera.")

